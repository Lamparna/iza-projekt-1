//
//  Simulator.swift
//  Simulator
//
//  Created by Filip Klembara on 17/02/2020.
//

import FiniteAutomata

/// Simulator
public struct Simulator {
    /// Finite automata used in simulations
    public let finiteAutomata: FiniteAutomata

    /// Initialize simulator with given automata
    /// - Parameter finiteAutomata: finite automata
    public init(finiteAutomata: FiniteAutomata) {
        self.finiteAutomata = finiteAutomata
    }

    /// Gets next state from current state with current string
    /// - Returns: Next automata state if found, nil otherwise
    private func getNextState(from: String, with: String) -> FiniteAutomataState?
    {
        return self.finiteAutomata.transitions.first { $0.from == from && $0.with == with };
    }
    
    /// Simulate automata on given string
    /// - Parameter string: string with symbols separated by ','
    /// - Returns: Empty array if given string is not accepted by automata,
    ///     otherwise array of states
    public func simulate(on string: String) -> [String] {
        let inputSymbols = string.split(separator: ",");
            
        var states : [String] = [];
        var currentState = self.finiteAutomata.initialState;
        var canEnd = self.finiteAutomata.finalStates.contains(currentState);
        states.append(currentState);
        
        for symbol in inputSymbols {
            if let nextState = getNextState(from: currentState, with: String(symbol))
            {
                currentState = nextState.to;
                canEnd = self.finiteAutomata.finalStates.contains(currentState);
                states.append(currentState);
            }
            else
            {
                return [];
            }
        }
        
        if canEnd
        {
            return states;
        }
        
        return [];
    }
    
    /// Check if transitions use defined symbols
    /// - Returns: True if all states are correct, false otherwise
    public func checkSymbols() -> Bool
    {
        for transitionState in self.finiteAutomata.transitions {
            let isValidSymbol = self.finiteAutomata.symbols.contains { transitionState.with == $0 }
            
            if !isValidSymbol
            {
                return false;
            }
        }
        
        return true;
    }
    
    /// Check if transitions use defined states
    /// - Returns: True if all states are correct, false otherwise
    public func checkStates() -> Bool
    {
        for transitionState in self.finiteAutomata.transitions
        {
            let isValidFromState = self.finiteAutomata.states.contains { transitionState.from == $0 }
            let isValidToState = self.finiteAutomata.states.contains { transitionState.to == $0 }
            
            if !isValidToState || !isValidFromState
            {
                return false;
            }
        }
        
        return true;
    }
    
    public func checkDeterminism() -> Bool
    {
        var uniqueStates: Set<FiniteAutomataState> = [];
        
        for transitionState in self.finiteAutomata.transitions
        {
            if uniqueStates.contains(where: { $0.from == transitionState.from && $0.with == transitionState.with })
            {
                return false;
            }
            
            uniqueStates.insert(transitionState);
        }
        
        return true;
    }
}
