//
//  FiniteAutomata.swift
//  FiniteAutomata
//
//  Created by Filip Klembara on 17/02/2020.
//

public struct FiniteAutomataState: Codable, Hashable {
    public let with: String
    public let to: String
    public let from: String
}

/// Finite automata
public struct FiniteAutomata: Codable {
    public let states: [String]
    public let symbols: [String]
    public let transitions: Set<FiniteAutomataState>
    public let initialState: String
    public let finalStates: [String]
}
